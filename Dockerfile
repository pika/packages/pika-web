FROM node:latest AS pika-angular-build

RUN npm update --location=global npm
RUN npm install -g @angular/cli@15.2.0

ARG FRONTEND_VERSION
# RUN wget https://gitlab.hrz.tu-chemnitz.de/pika/pika-web/-/archive/${FRONTEND_VERSION}/pika-web-${FRONTEND_VERSION}.tar.gz \
# && tar xzvf pika-web-${FRONTEND_VERSION}.tar.gz \
# && rm pika-web-${FRONTEND_VERSION}.tar.gz

RUN git clone --branch ${FRONTEND_VERSION} https://gitlab.hrz.tu-chemnitz.de/pika/pika-web.git pika-web-${FRONTEND_VERSION}

WORKDIR /pika-web-${FRONTEND_VERSION}/
RUN npm install
ARG FRONTEND_BASE_HREF
RUN ng build --configuration production --base-href=${FRONTEND_BASE_HREF}

FROM httpd:2.4 as pika-httpd

ARG FRONTEND_VERSION
COPY --from=pika-angular-build /pika-web-${FRONTEND_VERSION}/dist/pika-web/ /usr/local/apache2/htdocs/
COPY web_config/.htaccess /usr/local/apache2/htdocs/.htaccess
COPY web_config/httpd_v2.4.conf /usr/local/apache2/conf/httpd.conf
COPY web_config/merge_json.sh /usr/local/apache2/htdocs/assets/merge_json.sh
COPY config.json /usr/local/apache2/htdocs/assets/config.json

RUN chmod +x /usr/local/apache2/htdocs/assets/merge_json.sh
RUN chmod go+r /usr/local/apache2/htdocs/.htaccess
RUN apt update && apt install -y jq

WORKDIR /usr/local/apache2/htdocs/assets/

# merge config.json with appConfig.json
ENTRYPOINT ["./merge_json.sh"]
CMD ["httpd-foreground"]