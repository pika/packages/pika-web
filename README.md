# pika-web

```sh
git clone https://gitlab.hrz.tu-chemnitz.de/pika/packages/pika-web.git
```

1. Modify `.env` according to your settings.

```
vi .env
FRONTEND_VERSION="main"
FRONTEND_PORT=4200
FRONTEND_BASE_HREF=/
```

**Hint:**
If `pika-web` is not served at the default root URL ("/"), specify the base URL using the environment variable `FRONTEND_BASE_HREF`.

Example:

```
vi .env
# https://[your-domain]/job-monitoring
FRONTEND_BASE_HREF=/job-monitoring/
```

Copy `config.json.example` to `config.json` and modify it according to your settings.

```
cp config.json.example config.json

vi config.json
"backend": "http://127.0.0.1:4300" # replace with the corresponding ip address of pika-server
```

**Hint:**
If you use a proxy, the backend URL could look like this:

```
vi config.json
"backend": "https://pika.zih.tu-dresden.de/backend"
```

2. Install `pika-web`

```
./install.sh
```
