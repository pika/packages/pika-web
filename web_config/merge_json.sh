#!/bin/bash

# Get all content under "Table_Definition" from original appConfig.json
sed -n '/"Table_Definition":/,$p' appConfig.json > tables.json
sed -i '1s/^/{\n/' tables.json

# Append the contents of table_definitions to config.json and overwrite appConfig.json
jq -s '.[0] + .[1]' config.json tables.json > appConfig.json

exec "$@"
